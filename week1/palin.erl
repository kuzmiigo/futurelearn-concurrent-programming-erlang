-module(palin).
-export([palin/1,nopunct/1,palindrome/1,server/0]).

% palindrome problem
%
% palindrome("Madam I\'m Adam.") = true

palindrome(Xs) ->
    palin(nocaps(nopunct(Xs))).

nopunct([]) ->
    [];
nopunct([X|Xs]) ->
    case lists:member(X,".,\ ;:\t\n\'\"") of
	true ->
	    nopunct(Xs);
	false ->
	    [ X | nopunct(Xs) ]
    end.

nocaps([]) ->
    [];
nocaps([X|Xs]) ->
    [ nocap(X) | nocaps(Xs) ].

nocap(X) ->
    case $A =< X andalso X =< $Z of
	true ->
	    X+32;
	false ->
	    X
    end.

% literal palindrome

palin(Xs) ->
    Xs == reverse(Xs).

reverse(Xs) ->
    shunt(Xs,[]).

shunt([],Ys) ->
    Ys;
shunt([X|Xs],Ys) ->
    shunt(Xs,[X|Ys]).


%% Server

server() ->
    receive
        {Pid, {check, String}} ->
            io:format("Got check request, PID: ~p, string: ~s~n",
                      [Pid, String]),
            case palindrome(String) of
                true ->
                    Result = " is a palindrome";
                false ->
                    Result = " is not a palindrome"
            end,
            Pid ! {self(), {result, String ++ Result}},
            server();
        {Pid, _} ->
            io:format("Got stop request, PID: ~p~n", [Pid])
    end.

% PalinServ = spawn(palin, server, []).
% i().
% PalinServ ! {self(), {check, "Madam I\'m Adam"}}.
% receive {Pid, Reply} -> Reply end.
% flush().
% PalinServ ! {self(), stop}.
% i().
